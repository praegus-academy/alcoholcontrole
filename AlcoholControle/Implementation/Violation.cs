﻿using AlcoholControle.Interfaces;
using System;

namespace AlcoholControle.Implementation
{
    class Violation : IViolation
    {
        public double Promillage { get; private set;  }

        public Violation(double promillage)
        {
            Promillage = promillage;
        }
    }
}
