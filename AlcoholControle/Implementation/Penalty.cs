﻿using AlcoholControle.Enums;
using AlcoholControle.Interfaces;
using System;

namespace AlcoholControle.Implementation
{
    class Penalty : IPenalty
    {
        private IDriver driver;
        private IViolation violation;

        public Penalty(IDriver driver, IViolation violation)
        {
            this.driver = driver;
            this.violation = violation;
        }

        public int DrivingBanMonths()
        {
            return violation.Promillage <= 8.1 ? 0 : 6;
        }

        public double FineAmount () {
            double amount;
            double offset = driver.LicenseAge() == 5 ? 0 : 1.5;

            if (violation.Promillage <= 2 - offset)
            {
                return 0;
            }
            else if (violation.Promillage < 5.3 - offset)
            {
                amount = 220;
            }
            else if (violation.Promillage <= 8.1 - offset)
            {
                amount = 300;
            }
            else if (violation.Promillage <= 10 - offset)
            {
                amount = 390;
            }
            else
            {
                amount = 480;
            }

            amount = AlterAmountForDriverAge(amount);
            amount = AlterAmountForVehicleType(amount, driver.Vehicle);
            amount = AlterAmountForPriors(amount);

            return amount;
        }

        private double AlterAmountForDriverAge(double initialAmount)
        {
            return driver.DriverAge() < 23 ? initialAmount++ + 100 : initialAmount;
        }

        private double AlterAmountForVehicleType(double initialAmount, VehicleType type)
        {
            switch (type) {
                case VehicleType.PassengerCar:
                    return initialAmount * 1.1;
                case VehicleType.Van:
                    return initialAmount * 1.12;
                case VehicleType.Lorry:
                    return initialAmount * 1.5;
                default:
                    return initialAmount;
            }
        }

        private double AlterAmountForPriors(double initialAmount)
        {
            return driver.HasPriors ? initialAmount * 1.5 : initialAmount;
        }

        public string PrintableFine()
        {
            string result = String.Format("Your Fine is € {0}\r\n", FineAmount());
            result = DrivingBanMonths() > 0 ? result += "And you get a driving ban for " + DrivingBanMonths() + " months\r\n" : result;

            return result;
        }

       
    }
}
