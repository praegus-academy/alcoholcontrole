﻿using AlcoholControle.Enums;
using AlcoholControle.Interfaces;
using System;
using System.Globalization;

namespace AlcoholControle.Implementation
{
    class Driver : IDriver
    {
        private readonly DateTime LicenseStartDate;
        private readonly DateTime BirthDate;
        public bool HasPriors { get; private set; }
        public VehicleType Vehicle { get; private set; }
        

        public int LicenseAge()
        {
            return AgeByDate(LicenseStartDate);
        }
        public int DriverAge()
        {
            return AgeByDate(BirthDate);
        }

        public Driver (string LicenseStartDateStr, string BirthDateStr, bool Priors, VehicleType VehicleType)
        {
            LicenseStartDate = ParseDate(LicenseStartDateStr);
            BirthDate = ParseDate(BirthDateStr);
            HasPriors = Priors;
            Vehicle = VehicleType;
        }

        private DateTime ParseDate(string dateStr)
        {
            var cultureInfo = new CultureInfo("nl-NL");
            return DateTime.Parse(dateStr, cultureInfo, DateTimeStyles.NoCurrentDateDefault);
        }

        private int AgeByDate(DateTime date)
        {
            var today = DateTime.Today;
            var age = today.Year - date.Year;
            if (date.Date > today.AddYears(-age)) age--;

            return age;
        }
        
    }
}
