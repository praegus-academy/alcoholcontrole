﻿using AlcoholControle.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholControle.Interfaces
{
    interface IDriver
    {
        bool HasPriors { get; }
        int LicenseAge();
        int DriverAge();

        VehicleType Vehicle { get; }
    }
}
