﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholControle.Interfaces
{
    interface IPenalty
    {
        double FineAmount();
        int DrivingBanMonths();
        string PrintableFine();
      
    }
}
