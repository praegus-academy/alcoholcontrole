﻿using AlcoholControle.Enums;
using AlcoholControle.Implementation;
using AlcoholControle.Interfaces;
using System;

namespace AlcoholControle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Welcome to our finest fine calculator!");
            var startInputLine = Console.CursorTop;
            IDriver driver = GetDriverInformation();
            ConsoleUtil.ResetConsoleState(startInputLine);

            IViolation violation = GetViolationData();
            ConsoleUtil.ResetConsoleState(startInputLine);

            Console.WriteLine("Calculating penalty...");
            IPenalty penalty = new Penalty(driver, violation);
            
            Console.WriteLine(" ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(penalty.PrintableFine());
            Console.ResetColor();
            Console.WriteLine(" ");
            Console.WriteLine("Press any key to quit.");
            Console.ReadKey(true);
        }

        private static IViolation GetViolationData()
        {
            Console.WriteLine("Please provide the violation details...");
            Console.WriteLine("What is the measured alcohol promillage?");
            try
            {
                return new Violation(Double.Parse(Console.ReadLine()));
            }
            catch (FormatException e)
            {
                DisplayError(e.Message);
                return GetViolationData();
            }
        }

        private static IDriver GetDriverInformation()
        {
            Console.WriteLine("Please provide the driver's details...");
            Console.WriteLine("When was the driver's license acquired? (dd-MM-yyyy)");
            string licenseStartDate = Console.ReadLine();

            Console.WriteLine("What is the driver's birthday? (dd-MM-yyyy)");
            string birthDay = Console.ReadLine();

            Console.WriteLine("Does the driver have prior violations? (y/n)");
            char priorsInput = Console.ReadKey().KeyChar;
            bool hasPriors = priorsInput == 'y' || priorsInput == 'Y';

            VehicleType vehicleType = GetVehicleType();

            try
            {
                return new Driver(licenseStartDate, birthDay, hasPriors, vehicleType);
            } 
            catch (Exception e)
            {
                DisplayError(e.Message);
                return GetDriverInformation();
            }
        }

        private static VehicleType GetVehicleType()
        {
            Console.WriteLine("Please provide the vehicle's type:");
            Console.WriteLine("1 - Passenger Car");
            Console.WriteLine("2 - Van");
            Console.WriteLine("3 - Lorry");
            Console.WriteLine("4 - Moped");
            Console.WriteLine("9 - Other");
            char vehicleInput = Console.ReadKey(true).KeyChar;
            
            switch (vehicleInput)
            {
                case '1':
                    return VehicleType.PassengerCar;
                case '2':
                    return VehicleType.Van;
                case '3':
                    return VehicleType.Lorry;
                case '4':
                    return VehicleType.Moped;
                case '9':
                    return VehicleType.Other;
                default:
                    DisplayError("Unknown vehicle type. Please Try again!");
                    return GetVehicleType();
            }
        }

        private static void DisplayError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[ERROR] {0}", message);
            Console.ResetColor();
        }
    }
}
