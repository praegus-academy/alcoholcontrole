﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholControle.Enums
{
    enum VehicleType
    {
        PassengerCar,
        Van,
        Lorry,
        Moped,
        Other
    }
}
