﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholControle
{
    class ConsoleUtil
    {
        public static void ResetConsoleState(int lineToClearFrom)
        {
            for (var line = Console.CursorTop; line >= lineToClearFrom; line--)
            {
                Console.SetCursorPosition(0, line);
                Console.Write(new string(' ', Console.WindowWidth));
            }
            Console.SetCursorPosition(0, lineToClearFrom);
        }
    }
}
