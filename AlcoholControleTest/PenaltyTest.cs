﻿using AlcoholControle.Enums;
using AlcoholControle.Implementation;
using AlcoholControle.Interfaces;
using Moq;
using Xunit;

namespace AlcoholControleTest
{
    public class PenaltyTest
    {
        [Theory]
        [InlineData (8.1, 6)]
        [InlineData (8, 0)]
        [InlineData (8.2, 6)]
        public void EightPointOnePromilleOrMoreGivesASixMonthBan(double promillage, int expectedMonths)
        {
            //Arrange
            IDriver driver = new Driver("01-01-1970", "01-01-1970", false, VehicleType.PassengerCar);
            IViolation violation = new Violation(promillage);

            //Act
            IPenalty penalty = new Penalty(driver, violation);
            int actualMonths = penalty.DrivingBanMonths();

            //Assert
            Assert.Equal(expectedMonths, actualMonths);
        }

        [Fact]
        public void DriverUnderTwentyThreeGetsAHigherFine()
        {
            //Arrange

            var mockDriver = new Mock<IDriver>();
            mockDriver.Setup(driver => driver.Vehicle).Returns(VehicleType.Other);
            mockDriver.SetupSequence(driver => driver.DriverAge())
                .Returns(22)
                .Returns(23);
            
            var mockViolation = new Mock<IViolation>();
            mockViolation.Setup(violation => violation.Promillage).Returns(8);

            //Act
            IPenalty penalty = new Penalty(mockDriver.Object, mockViolation.Object);

            double firstFine = penalty.FineAmount();
            double secondFine = penalty.FineAmount();

            //Assert
            Assert.Equal(firstFine, secondFine + 100);
        }
    }
}
