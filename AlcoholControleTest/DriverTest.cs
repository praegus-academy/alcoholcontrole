using System;
using Xunit;

using AlcoholControle.Interfaces;
using AlcoholControle.Implementation;
using AlcoholControle.Enums;

namespace AlcoholControleTest
{
    public class DriverTest
    {
        [Theory]
        [InlineData ("01-01-1970", 51)]
        public void TheDriverLicenseAgeCanBeCalculatedCorrectly(string issueDate, int expectedAge)
        {
            //Arrange
            IDriver driver = new Driver(issueDate, "01-01-1970", false, VehicleType.Other);
            
            //Act
            int actualAge = driver.LicenseAge();
            
            //Assert
            Assert.Equal(expectedAge, actualAge);
        }

        [Theory]
        [InlineData("01-01-1980", 41)]
        public void TheDriverAgeCanBeCalculatedCorrectly(string driverDate, int expectedAge)
        {
            //Arrange
            IDriver driver = new Driver("01-01-1970", driverDate, false, VehicleType.Other);

            //Act
            int actualAge = driver.DriverAge();

            //Assert
            Assert.Equal(expectedAge, actualAge);

        }
    }
}
